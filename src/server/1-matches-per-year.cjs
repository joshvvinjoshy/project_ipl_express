const matches = require('../data/allmatches.json');
const fs = require('fs');
const express = require('express')
const router = express.Router()

let match_dict = {};
// function matchesperyear(){   
router.get('/', (req,res)=>{
    matches.map((match) =>{
        const year = match.season;
        if(match_dict[year] == undefined){
            match_dict[year] = 1;
        }
        else{
            match_dict[year]++;
        }
    });
    // console.log(match_dict);
    res.json(match_dict)
    // const result = JSON.stringify(match_dict, null, 2) + '\n';
    // res.send(result)
    // fs.writeFileSync("../public/output/matchesPerYear.json", res, 'utf-8');
    // console.log(res);

})
// matchesperyear();
module.exports = router
// module.exports = match_dict