const matches = require('../data/allmatches.json');
// const match_dict = require('./1-matches-per-year.cjs')
const fs = require('fs');
const express = require('express')
const router = express.Router()
// console.log(match_dict);
router.get('/', (req,res)=>{
    let match_d = {};
    matches.map((match) => {
        match_d[match.season] = {}; 
    });
    matches.map((match) => {
        const year = match.season;
        const winner = match.winner;
        if(winner == ""){
            return true;
        }
        if(match_d[year][winner] == undefined){
            match_d[year][winner] = 1;
        }
        else{
            match_d[year][winner] ++;
        }
    });
    // console.log(match_d);
    const result = JSON.stringify(match_d, null, 2) + '\n';
    res.json(match_d)
    // fs.writeFileSync("../public/output/matchesWonPerTeamPerYear.json", res, 'utf-8');
    // console.log(res);
})
// matcheswonperteam();
module.exports = router