const matches = require('../data/allmatches.json');
const deliveries = require('../data/alldeliveries.json');
// const imports = require('./1-matches-per-year.cjs');
const fs = require('fs');
// console.log(match_dict);
const express = require('express')
const router = express.Router()
router.get('/', (req,res)=>{
    let players = {};
    let answer = {};
    let match_dict = {}
    matches.map((match) =>{
        const year = match.season;
        if(match_dict[year] == undefined){
            match_dict[year] = {}
        }
    });
    const matches_list = Object.entries(match_dict);
    matches_list.map(([year,value]) =>{
        // console.log(year);
        players[year] = {};
        answer[year] = {};
    });
    matches.map((match) => { 
        const year = match.season;
        const player_of_match = match.player_of_match;
        if(player_of_match == ''){
            return true;
        }
        if(players[year][player_of_match] == undefined){
            players[year][player_of_match] = 1;
        }
        else{
            players[year][player_of_match] ++;
        }
    });
    const players_list = Object.entries(players);
    players_list.map(([year, value]) =>{
        let bestplayer = {};
        const keyValueArray = Object.entries(value);
        keyValueArray.sort((a, b) => b[1] - a[1]); 
        const sortedObject = Object.fromEntries(keyValueArray);   
        let ind =0;
        let max = 0;
        Object.entries(sortedObject).map(([player, no_of_awards])=>{
            if(ind == 0){
                max = no_of_awards;
            }
            if(no_of_awards == max){
                max = no_of_awards;
                answer[year][player] = max;
            }
            ind++;            
        });
    });
    // console.log(answer);
    res.json(answer)
    // const res = JSON.stringify(answer, null, 2) + '\n';
    // fs.writeFileSync("../public/output/playerOfMatch.json", res, 'utf-8');
    // console.log(res);
})
// playerofmatch();
module.exports = router