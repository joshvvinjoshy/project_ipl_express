const Papa = require('papaparse');
const express = require('express')
const app = express()
const path = require('path')
const router = express.Router()
const fs = require('fs')
// const {readFile, writeFile} = require('fs').promi
const port = process.env.PORT || 8000
// function convertCsvToJson(csvFilePath) {
//   const csvData = fs.readFileSync(csvFilePath, 'utf8');
//   const parsedData = Papa.parse(csvData, {
//     header: true,
//     skipEmptyLines: true,
//   });
//   // console.log(parsedData.meta);
//   return parsedData.data;
// }

// const csvFilePath1='/home/joshvvinjoshy/mb/project_ipl/src/data/deliveries.csv'
// const csvFilePath2='/home/joshvvinjoshy/mb/project_ipl/src/data/matches.csv'
// const deliveries = convertCsvToJson(csvFilePath1);
// const matches = convertCsvToJson(csvFilePath2);
// const allmatches = JSON.stringify(matches, null, 2);
// const alldeliveries = JSON.stringify(deliveries, null, 2);
// fs.writeFileSync('/home/joshvvinjoshy/mb/project_ipl/src/data/alldeliveries.json', alldeliveries);
// fs.writeFileSync('/home/joshvvinjoshy/mb/project_ipl/src/data/allmatches.json', allmatches);

// console.log(deliveries);
// console.log(matches);
// module.exports = {matches, deliveries};
app.use('/matches_per_year', require('../server/1-matches-per-year.cjs'))
app.use('/matches_won_per_team_per_year', require('../server/2-matches-won-per-team-per-year.cjs'))
app.use('/extra_runs', require('../server/3-extra-runs.cjs'))
app.use('/economical_bowler', require('../server/4-economical-bowler.cjs'))
app.use('/won_toss_won_match', require('../server/5-won-toss-won-match.cjs'))
app.use('/player_of_match', require('../server/6-player-of-match.cjs'))
app.use('/strike_rate', require('../server/7-strike-rate.cjs'))
app.use('/highest_dismissals', require('../server/8-highest-dismissals.cjs'))
app.use('/bowler_with_best_economy_in_super_overs', require('../server/9-bowler-with-best-economy-in-super-overs.cjs'))

// app.get('/matches_per_year')
// app.get('/matches_won_per_team_per_year')
// app.get('/extra_runs')
// app.get('/economical_bowler')
// app.get('/won_toss_won_match')
// app.get('/player_of_match')
// app.get('/strike_rate')
// app.get('/highest_dismissals')
// app.get('/bowler_with_best_economy_in_super_overs')
app.listen(port, ()=>{
  console.log(`Server is listening on ${port}`)
})